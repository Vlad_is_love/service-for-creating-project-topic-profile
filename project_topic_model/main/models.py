from django.db import models
from django.urls import reverse


# Create your models here.
class Project(models.Model):
    title = models.CharField('Название', max_length=255)
    description = models.TextField('Описание')
    topic_1 = models.TextField('Знать', null=True)
    topic_2 = models.TextField('Уметь', null=True)
    topic_3 = models.TextField('Владеть', null=True)
    sparsity_phi = models.FloatField('Разреженность фи', null=True)
    sparsity_theta = models.FloatField('Разреженность тетта', null=True)
    kernel_contrast = models.FloatField('Контраст темы', null=True)
    kernel_purity = models.FloatField('Чистота темы', null=True)
    perplexity = models.FloatField('Перплексия', null=True)
    chosen_jobs = models.JSONField('Выбранные професии', null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'

    def get_absolute_url(self):
        return reverse('project_profile', kwargs={'id': self.pk})


class ProjectFile(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    document = models.FileField('Проектный документ', upload_to='main/uploads/')

    class Meta:
        verbose_name = 'Проектный документ'
        verbose_name_plural = 'Проектные документы'


class DescriptionModel(models.Model):
    job_name = models.CharField("Название профессии", max_length=255, null=True)
    job_json = models.FileField('Дескрипторная модель профессии', upload_to='main/uploads/jobs/')

    class Meta:
        verbose_name = 'Дескрипторная модель'
        verbose_name_plural = 'Дескрипторные модели'
