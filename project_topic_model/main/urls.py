from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name='main'),
    path('add_documents_form', views.document_addition, name='doc_form'),
    path('successful_upload', views.success, name='success'),
    path('project_profile/<int:id>/', views.project_profile, name='project_profile'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
