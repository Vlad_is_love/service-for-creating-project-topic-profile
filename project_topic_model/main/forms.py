from .models import Project, ProjectFile, DescriptionModel
from django.forms import ModelForm, TextInput, Textarea, FileInput, SelectMultiple
from .functions import get_job_names

CHOICES = get_job_names()

class DocumentForm(ModelForm):
    class Meta:
        model = Project
        fields = ['title', 'description', 'chosen_jobs']
        choices = [("dfs", 'hgd'), ("dfs", 'hgd')]
        widgets = {
            'title': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите название проекта'
            }),
            'description': Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Введите описание проекта'
            }),
            'chosen_jobs': SelectMultiple(attrs={
                'class': 'form-control',
            }, choices=CHOICES)
        }


class FileDocumentForm(ModelForm):
    class Meta:
        model = ProjectFile
        fields = ['document']
        widgets = {
            'document': FileInput(attrs={
                'placeholder': 'Выберите проектный документ',
                'type': 'file',
                'name': 'file',
                'class': 'form-control',
                'accept': 'text/csv,.txt,.pdf',
                'multiple': 'True',
            })
        }
