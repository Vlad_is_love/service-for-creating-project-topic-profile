from django.shortcuts import render, redirect

from .functions import get_topic_profile
from .forms import DocumentForm, FileDocumentForm
from .models import ProjectFile, Project, DescriptionModel
from threading import Thread


# Create your views here.
def document_addition(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST)
        file_form = FileDocumentForm(request.POST, request.FILES)
        files = request.FILES.getlist('document')
        if form.is_valid() and file_form.is_valid():
            project_instance = form.save()
            for file in files:
                file_instance = ProjectFile(document=file, project=project_instance)
                file_instance.save()
            get_topic_profile(project_instance.pk)
            return redirect(project_instance.get_absolute_url())
    else:
        form = DocumentForm()
        file_form = FileDocumentForm()
        descs = DescriptionModel.objects.all()
    context = {
        'form': form,
        'file_form': file_form,
        'choices': descs,
    }
    return render(request, 'main/docs_form.html', context)


def index(request):
    projects = Project.objects.all()
    context = {
        'projects': projects
    }
    return render(request, 'main/index.html', context)


def success(request):
    return render(request, 'main/success_upload.html')


def project_profile(request, id):
    project = Project.objects.get(pk=id)
    documents = ProjectFile.objects.filter(project=project)
    context = {
        'title': project.title,
        'description': project.description,
        'documents': documents,
        'topic1': project.topic_1,
        'topic2': project.topic_2,
        'topic3': project.topic_3,
        'sparsity_phi': project.sparsity_phi,
        'sparsity_theta': project.sparsity_theta,
        'kernel_contrast': project.kernel_contrast,
        'kernel_purity': project.kernel_purity,
        'perplexity': project.perplexity,
    }
    return render(request, 'main/project_profile.html', context)
