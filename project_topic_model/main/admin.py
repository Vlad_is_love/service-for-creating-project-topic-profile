from django.contrib import admin
from .models import Project, ProjectFile, DescriptionModel
# Register your models here.


class ProjectFileInline(admin.TabularInline):
    model = ProjectFile


class ProjectAdmin(admin.ModelAdmin):
    inlines = [
        ProjectFileInline
    ]


admin.site.register(Project, ProjectAdmin)
admin.site.register(DescriptionModel)
