import pandas as pd
from natasha import MorphVocab
from natasha import (
    Segmenter,

    NewsEmbedding,
    NewsMorphTagger,
    NewsSyntaxParser,

    Doc
)
import nltk
from nltk.corpus import stopwords
import os
from .models import ProjectFile, Project, DescriptionModel
import re
import artm
import shutil
from pathlib import Path
import json
import ast

nltk.download('stopwords')
punctuation = ['.', ',', ':', ';', '!', '?', '"', '\'', '[', ']', '/', '(', ')', '&', '%', '«', '»', '—', '-', 'quot', 'u200b']


def preprocess(id):
    project = Project.objects.get(pk=id)
    documents = ProjectFile.objects.filter(project=project)
    texts = []
    for document in documents:
        filename, file_extension = os.path.splitext(document.document.name)
        if '.csv' == file_extension:
            f = open(document.document.name, 'r')
            lines = f.readlines()
            f.close()
        text = get_together(lines)
        doc = tokenize(text)
        text_in_base_form = to_base_form(doc)
        resulted_text = remove_stop_words(text_in_base_form)
        texts.append(resulted_text)
    data = to_vowpal_wabbit(texts)
    result = ''
    for i in range(len(data)):
        temp = to_vw_format(data[i])
        if '|text \n' != temp:
            result += temp
    return result


def get_together(document):
    res = ""
    for each in document:
        res += ''.join(each) + ' '
    return res


def tokenize(raw_text):
    segmenter = Segmenter()
    emb = NewsEmbedding()
    morph_tagger = NewsMorphTagger(emb)
    syntax_parser = NewsSyntaxParser(emb)
    doc = Doc(raw_text)
    doc.segment(segmenter)
    doc.tag_morph(morph_tagger)
    doc.parse_syntax(syntax_parser)
    return doc


def to_base_form(doc):
    morph_vocab = MorphVocab()
    result = []
    for token in doc.tokens:
        token.lemmatize(morph_vocab)
        result.append(token.lemma)
    return result


def remove_stop_words(raw_text):
    stopWords = set(stopwords.words('russian'))
    text = raw_text
    res = []
    for word in text:
        if word not in stopWords and word not in punctuation:
            res.append(word)
    return res


def to_vowpal_wabbit(texts):
    courses_res = []
    for each in texts:
        temp_str = ""
        counter = 0
        for word in each:
          temp_str += word + ' '
          counter += 1
          if 10 == counter:
            courses_res.append(temp_str)
            temp_str = ""
            counter = 0
        courses_res.append(temp_str)
    return courses_res


def to_vw_format(document, label=None):
    return str(label or '') + '|text ' + ' '.join(re.findall('\w{3,}', document.lower())) + '\n'


def get_topics(id):
    batch_vectorizer = artm.BatchVectorizer(data_path='main/temp_data/preprocessed_data_%d.txt' % id, data_format='vowpal_wabbit',
                                            target_folder='main/temp_data/batches_%d' % id, batch_size=10)
    dictionary = artm.Dictionary(data_path='main/temp_data/batches_%d' % id)
    model = artm.ARTM(num_topics=3,
                      num_document_passes=10,
                      dictionary=dictionary,
                      scores=[artm.TopTokensScore(name='TopTokensScore', num_tokens=8),
                              artm.PerplexityScore(name='PerplexityScore')],
                      regularizers=[artm.SmoothSparseThetaRegularizer(name='SparseTheta',
                                                                      tau=-0.15)]
                      )
    model.scores.add(artm.SparsityPhiScore(name='SparsityPhiScore'))
    model.scores.add(artm.SparsityThetaScore(name='SparsityThetaScore'))
    model.scores.add(artm.TopicKernelScore(name='TopicKernelScore',
                                                probability_mass_threshold=0.3))
    model.regularizers.add(artm.SmoothSparsePhiRegularizer(name='SparsePhi', tau=-0.1))
    model.regularizers.add(artm.DecorrelatorPhiRegularizer(name='DecorrelatorPhi', tau=1.5e+5))
    model.fit_offline(batch_vectorizer=batch_vectorizer, num_collection_passes=10)
    result = {}
    for topic_name in model.topic_names:
        result[topic_name] = model.score_tracker['TopTokensScore'].last_tokens[topic_name]
    result['sparsity_phi'] = model.score_tracker['SparsityPhiScore'].last_value
    result['sparsity_theta'] = model.score_tracker['SparsityThetaScore'].last_value
    result['kernel_contrast'] = model.score_tracker['TopicKernelScore'].last_average_contrast
    result['kernel_purity'] = model.score_tracker['TopicKernelScore'].last_average_purity
    result['perplexity'] = model.score_tracker['PerplexityScore'].last_value
    os.remove("main/temp_data/preprocessed_data_%d.txt" % id)
    shutil.rmtree('main/temp_data/batches_%d' % id)
    path = Path('.')
    for f in path.glob('bigartm.*'):
        os.remove(f)
    return result


def get_topic_profile(id):
    preprocessed_data = preprocess(id=id)
    with open("main/temp_data/preprocessed_data_%d.txt" % id, "w") as file:
        file.write(preprocessed_data)
    topics = get_topics(id)
    project = Project.objects.get(pk=id)
    topics_str = []
    topics_list = [topics.get('topic_0'), topics.get('topic_1'), topics.get('topic_2')]
    job_names = ast.literal_eval(str(project.chosen_jobs))
    matched_topics_list = match(job_names, topics_list)
    for topic in matched_topics_list:
        temp = ''
        for word in topic:
            temp += word + ", "
        temp = temp[:-2]
        topics_str.append(temp)
    project.topic_1 = topics_str[0]
    project.topic_2 = topics_str[1]
    project.topic_3 = topics_str[2]
    project.sparsity_phi = topics.get('sparsity_phi')
    project.sparsity_theta = topics.get('sparsity_theta')
    project.kernel_contrast = topics.get('kernel_contrast')
    project.kernel_purity = topics.get('kernel_purity')
    project.perplexity = topics.get('perplexity')
    project.save()


def match(job_names, topics_list):
    desc_models = []
    for job_name in job_names:
        desc_models.append(DescriptionModel.objects.get(job_name=job_name))

    matched_topic_list = {"Знать": [], "Уметь": [], "Владеть": []}

    for desc_model in desc_models:
        doc_link = desc_model.job_json.name
        doc = open(doc_link, encoding="utf-8")
        data = json.load(doc)
        descriptions = data['children']
        for description in descriptions:
            competencies = description['children']
            for competence in competencies:
                key_words = competence['children']
                for key_word in key_words:
                    for topic in topics_list:
                        if key_word["name"] in topic:
                            print(description['name'])
                            temp_list = list(matched_topic_list[description['name']])
                            temp_list.append(competence['name'])
                            matched_topic_list[description['name']] = set(temp_list)
        doc.close()

    return matched_topic_list.values()


def get_job_names():
    descs = DescriptionModel.objects.all()
    choices_list = []
    for desc in descs:
        choices_list.append((desc.job_name, desc.job_name))
    return choices_list