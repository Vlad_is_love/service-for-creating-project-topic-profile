# Generated by Django 4.0.2 on 2022-03-07 10:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_remove_descriptionmodel_название профессии_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='chosen_jobs',
            field=models.JSONField(null=True, verbose_name='Выбранные професии'),
        ),
    ]
