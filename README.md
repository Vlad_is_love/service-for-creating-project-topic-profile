# Service for creating project topic profile

Web-service made for creating project topic profile using project's documentation.
Project topic profile can be used to understand what competencies should future participants has in order to be useful.

Made by Vladislav Shevtsov as his bachelor diploma
